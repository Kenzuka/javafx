# JavaFX


____________________________________________________________________________________________________________________________________
**Screenshot node**


* By scene
```
public void takeSnapShot(Scene scene){
    Platform.runLater(new Runnable() {
        @Override
        public void run() {
            WritableImage writableImage = new WritableImage((int)scene.getWidth(), (int)scene.getHeight());
            scene.snapshot(writableImage);
            File file = new File("snapshot.png");
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
            } catch (IOException ex) {}
        }
    }); 
  }
```





*  By fxid ("#fxid"):
```
  public void takeSnapShot(String nodeId){
      Node node = (Node) scene.lookup(nodeId);
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
            WritableImage image = node.snapshot(new SnapshotParameters(), null);
            File file = new File("node.png");

            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException e) {}
        }
    });
  }
```
____________________________________________________________________________________________________________________________________
